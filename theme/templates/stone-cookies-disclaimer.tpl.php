<div class="Container Container--cookies">
  <div class="Container-inner">
    <div class="Box Box--cookies">
      <?php if (!empty($title)): ?>
        <h2 class="Box-title">
          <?php print $title; ?>
        </h2>
      <?php endif; ?>

      <div class="Box-content">
        <div class="CookieMessage">
          <?php print $message; ?>
        </div>

        <ul class="CookieOptions">
          <?php if (!empty($more_link)): ?>
            <li class="CookieOptions-more">
              <?php print $more_link; ?>
            </li>
          <?php endif; ?>

          <li class="CookieOptions-continue">
            <?php print $continue_link; ?>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
